package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static int [] CreateMas()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input n: ");
        int n = scanner.nextInt();

        return new int[n];
    }

    static void PrintMas(int[] mas)
    {
        for (int i=0; i<mas.length;i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.println();
    }

    //for 1

    static void FillMas1Random(int[] mas)
    {
        Random rnd = new Random();

        int min = 150;
        int max = 200;
        int diff = max - min;

        int k;

        int[] mas1 = new int [2];
        mas1[0] = -1;
        mas1[1] = 1;


        for (int i=0; i<mas.length;i++)
        {
            k = rnd.nextInt(2) ;
            mas[i]=(rnd.nextInt(diff + 1) + min)*mas1[k];
        }
    }

    static int CalcSredRostOfBoys(int[] mas)
    {
        int sumBoys=0, sumRostBoys=0;

        for (int i=0; i<mas.length;i++)
        {
            if(mas[i]<0)
            {
                sumRostBoys+=mas[i];
                sumBoys++;
            }
        }
        return sumRostBoys/sumBoys;
    }
    static int CalcSredRostOfGirls(int[] mas)
    {
        int sumGirls=0, sumRostGirls=0;

        for (int i=0; i<mas.length;i++)
        {
            if(mas[i]>0)
            {
                sumRostGirls+=mas[i];
                sumGirls++;
            }
        }
        return sumRostGirls/sumGirls;
    }

    //for 2

    static void FillMasRandom(int[] mas)
    {
        Random rnd = new Random();

        for (int i=0; i<mas.length;i++)
        {
            mas[i]=rnd.nextInt(20);
        }
    }

    static void FindRepeatNumAndInd(int[] mas)
    {
        for (int i=0;i<mas.length-1;i++)
        {
            for(int j=i+1; j<mas.length; j++)
            {
                if(mas[i]==mas[j])
                {
                    System.out.println(mas[i]+" = "+ mas[j]+": "+i+" , "+j);
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] mas = CreateMas();

        //1
        /*FillMas1Random(mas);
        PrintMas(mas);
        int sredBoysRost = CalcSredRostOfBoys(mas);
        int sredGirlsRost = CalcSredRostOfGirls(mas);
        System.out.println("Sredniy rost of boys: "+sredBoysRost);
        System.out.println("Sredniy rost of girls: "+sredGirlsRost);*/

        //2
        FillMasRandom(mas);
        PrintMas(mas);
        FindRepeatNumAndInd(mas);
    }
}
